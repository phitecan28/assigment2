#include <stdio.h>

int main()
{
    int pilihMenu;
    float inputNumber;
    float hasil = 0;
    
    printf("====================================\n");
    printf("Program Kalkulator Sederhana\n");
    printf("====================================\n\n");
    printf("Masukan Angka : ");
    scanf("%f",&hasil);

    printf("Pilih Menu\n ");
    printf("1. Penjumlahan (+)\n ");
    printf("2. Pengurangan (-)\n ");
    printf("3. Perkalian (*)\n ");
    printf("4. Pembagian (/)\n ");
    printf("5. Lihat Hasil \n ");
    
    printf("Pilih Menu : ");
    scanf("%d",&pilihMenu);
    
    
    while(pilihMenu != 5) {
        switch (pilihMenu) {
            case 1: 
            printf("Anda Memilih Operator Penjumlahan \n");
            printf("Masukan angka: ");
            scanf("%f",&inputNumber);
            hasil = hasil + inputNumber;
            printf("%0.2f \n",hasil);
            break;
            
            case 2: 
            printf("Anda Memilih Operator Pengurangan \n");
            printf("Masukan angka: ");
            scanf("%f",&inputNumber);
            hasil = hasil - inputNumber;
            printf("%0.2f \n",hasil);
            break;
            
            case 3: 
            printf("Anda Memilih Operator Perkalian \n");
            printf("Masukan angka: ");
            scanf("%f",&inputNumber);
            hasil = hasil * inputNumber;
            printf("%0.2f \n",hasil);
            break;
            
            case 4: 
            printf("Anda Memilih Operator Pembagian \n");
            printf("Masukan angka: ");
            scanf("%f",&inputNumber);
            hasil = hasil / inputNumber;
            printf("%0.2f \n",hasil);
            break;
        }
        printf("Pilih Menu : ");
        scanf("%d",&pilihMenu);
    }
    
    printf("-------------------------------\n");
    printf("hasil: %0.2f", hasil);
    printf("\n-------------------------------\n");

    return 0;
}
