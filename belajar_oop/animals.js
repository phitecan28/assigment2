class Animal{
    _nafas;
    constructor(name, hungry,age,_nafas){
        this.name = name;
        this.hungry = hungry;
        this.age = age;
        this._nafas = _nafas;
        
    }
    doEat(){
        console.log("Makan Bang");

    }
    doBreath(){
        if(this._nafas == "true"){
            console.log("Mari Bernafas");
        }else{
            console.log("Sesak Nafas");
        }
    }
    doMove(){
        console.log("Mari Bergerak");
    }
}

    class Bird extends Animal {
    
        constructor(name,hungry,age) {
        super(name);
        this.hungry = hungry;
        this.age =age;

  
        }
    
        // ...
        doFly(){
            console.log("Terbang");

        }
    }

  class Dog extends Animal {

  
    constructor(name,hungry,salto,_nafas) {
      super(name);
      this.hungry = hungry;
      this.salto = salto;
      this._nafas= _nafas;
      
    
    }
    
    // ...
    doHow(){
        console.log("Ya Elah");

    }


  }

const Burung = new Bird("Burung","Laparrr","20 Tahun");
const Bull = new Dog("Bull","Tidak Lapar","Salto","true");
const Gadis = new Dog("Gadis","Tidak Lapar","Salto","false");


Bull.doHow();
Bull.doBreath();
Gadis.doHow();
Gadis.doBreath();
Burung.doEat();